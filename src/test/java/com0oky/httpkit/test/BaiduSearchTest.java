/**
 * @author mdc
 * @date 2017年7月5日 下午12:47:40
 */
package com0oky.httpkit.test;

import org.junit.Assert;
import org.junit.Test;

import com0oky.httpkit.http.HttpKit;

/**
 * @author mdc
 * @date 2017年7月5日 下午12:47:40
 */
public class BaiduSearchTest {
	
	String url = "https://baidu.com";
	
	//请求百度首页 
	//注意,这里会自动处理https单向认证, 可以直接请求https
	@Test
	public void baiduIndexTest(){
		String html = HttpKit.get(url).execute().getString();
		System.out.println(html);
		Assert.assertTrue(html.contains("百度一下，你就知道"));
	}
	
	//百度搜索 关键字 oschina
	@Test
	public void baiduSearchTest(){
		String format = String.format(url + "/s?wd=%s&tn=98012088_5_dg&ch=11", "oschina");
		String html = HttpKit.get(format).execute().getString();
		System.out.println(html);
	}
	
	//百度搜索 关键字 oschina, 并写入文件
	@Test
	public void baiduSearchWriteFileTest(){
		String format = String.format(url + "/s?wd=%s&tn=98012088_5_dg&ch=11", "oschina");
		HttpKit.get(format).execute().transferTo("d:/baiduSearchResult.html");
	}
}
