## httpKit - 极简、极速的Http Client

- 基于Apache httpclient 4.3.x封装,
- 支持多线程异步请求
- 加入请求连接池
- 统一的参数设置
- 管道式API设计
- 支持双向认证
- 可重复读的Response
- 对Response简单的结果处理(写文件,写流,转Json对象,转String,转byte[])
- 文件上传、下载
- 对重定向路径处理,网络代理
- 很简单的实现会话保持
- 简单的认证功能实现
- 简单的失败重试机制实现
- 暴露用户自定义拦截器
- 所有这些只有一个HttpKit入口,对! 很简单


### 使用的第一步

> `HttpKit`的第一步是这么简单而又愉悦的开始了

``` java
HttpKit.get(url); //创建get请求
HttpKit.post(url); //创建post请求
HttpKit.delete(url); //创建delete请求
HttpKit.put(url); //创建put请求
```

在这里,`HttpKit`使用静态方法的方式提供api。不必使用new来创建`HttpKit`对象。实际上在`HttpKit`上所有的请求操作也都是从这里开始的。
`HttpKit`目前提供`get`、`post`、`delete`、`put`这四种请求方式，每一种请求方式都提供4个方法的重载。


以`HttpKit.post`为例

```java
RequestBase post(String url); // ①
RequestBase post(URI uri); // ②
RequestBase post(String url, RequestBase request); //③
RequestBase post(URI uri, RequestBase request); //④
```

- ① 使用一个url字符串请求
- ② 使用URI对象请求
- ③ 使用一个url字符串, 并且和一个`RequestBase`对象作为本次请求
- ④ 使用URI对象请求, 并且和一个`RequestBase`对象作为本次请求

`注`：对于本次请求，会把传入`RequestBase`对象的`org.apache.http.client.config.RequestConfig.Builder`、`org.apache.http.client.CookieStore`和`org.apache.http.Header[]`作为本次请求的初始化数据，这样就优雅的实现Session的保持，使会话轻易传到下一次请求中。对于需要先登录的请求使用这个方法对你很有帮助。另外，请求方法会自动识别`https`请求，自动处理单向认证。对于`https`双向认证请求，需要自行设置证书，可以参阅`RequestBase.setJks`。

### RequestBase对象

> `HttpKit.post(url)`返回的是`RequestBase`对象，这个对象提供参数设置、代理、`Cookie`、`Header`、`userAgent`、 `ContentType`、 认证、 单双向认证、上传文件、失败重试等配置操作，另外还提供发起请求操作。


### 设置请求参数

> 使用`RequestBase`设置请求参数，`httpKit`使用大量的方法重载来实现参数设置，尽可能的对参数处理难度降到最低。

设置请求参数有三种方式：
- 使用`addParameter`来向参数列表中追加参数
- 使用`setParameters`设置参数，这个方法会清空之前设置的所有参数，包括之前追加的参数列表，所以要根据实际情况而定
- 使用useForm方法设置参数，这个方法会抛弃之前设置或追加的参数，转而使用模拟表单的方式提交参数，所以这个方法可以使用浏览器的方式上传文件


> 设置参数方法重载

> ![设置参数方法重载](https://git.oschina.net/uploads/images/2017/0707/172232_d345fdea_503982.png "设置参数方法重载")

> 追加参数方法重载

> ![追加参数方法重载](https://git.oschina.net/uploads/images/2017/0707/172338_5f52a01b_503982.png "追加参数方法重载")

举个栗子

```java
HttpKit.post("https://0oky.com").addParameter("name", "value");
```

`注：对于设置的value可以是任意的对象，在设置参数时内部会调用对象的toString()方法`



上传文件

```java
FormPart form = FormPart.create("name", "value");
form.addParameter("fieldName", new File("test.txt"));
HttpKit.post("https://0oky.com").useForm(form);
```

> 对于`FormPart`对象仅仅是设置表单参数和编码，当然设置参数的过程是链式的。例如：
>  ```java 
FormPart form = FormPart.create()
.addParameter("name", "value")
.addParameter("fieldName", new File("test.txt"))
.addParameter("name2", "testString".getBytes());
```


### Cookie操作

> 使用`RequestBase.addCookie方法`设置多个`Cookie`,由于是一个可变参数，所以你可以传递任意多个Cookie对象。
> 另外api提供`setCookieStore`和`getCookieStore`用来获取、设置、清除、过期 `Cookie`的高级操作

```java
HttpKit.post("https://0oky.com").addCookie(new BasicClientCookie("name", "value"));
HttpKit.post("https://0oky.com").addCookie(new BasicClientCookie("name", "value"), new BasicClientCookie("name2", "value2"));
``` 

### Header操作
> 使用`RequestBase.xxxHeader方法`可以对Header操作,方法如下
![Header操作](https://git.oschina.net/uploads/images/2017/0707/171011_a2fb5b88_503982.png "Header操作方法")


### 设置网络代理

> 使用`RequestBase.setProxy`方法可以设置网络代理,同样提供多个重载方法,如下
![setProxy重载方法](https://git.oschina.net/uploads/images/2017/0707/165838_c510f28e_503982.png "setProxy重载方法")

举个栗子

``` java
HttpKit.post("https://0oky.com").setProxy("192.168.10.10", 1182);
```

### 设置请求失败时重试机制

> 使用`RequestBase.setRetryTimes`方法可以设置网络代理,同样提供多个重载方法,可以自定义重试处理器具如下
![请求失败时重试](https://git.oschina.net/uploads/images/2017/0707/170055_b0517160_503982.png "请求失败时重试机制")


### 关于`https`双向认证

>使用`RequestBase.setJKS`即完成双向认证的证书设置，主要提供证书文件和密码即可，方法重载如下：
![双向认证](https://git.oschina.net/uploads/images/2017/0707/172953_68a467e2_503982.png "双向认证")

### 关于认证方案

> 设置认证方案provider, 使用`RequestBase.setCredentials`方法实现。目前认证方案有
> - Basic: Basic认证方案是在RFC2617号文档中定义的。这种授权方案用明文来传输凭证信息，所以它是不安全的
> - Digest: Digest（摘要）认证方案是在RFC2617号文档中定义的。Digest认证方案比Basic方案安全的多
> - NTLM: NTLM认证方案是个专有的认证方案，由微软开发，并且针对windows平台做了优化。NTLM被认为比Digest更安全。
> - SPNEGO/Kerberos : SPNEGO(Simple and Protected GSSAPI Negotiation Mechanism)是GSSAPI的一个“伪机制”，它用来协商真正的认证机    制。SPNEGO最明显的用途是在微软的HTTP协商认证机制拓展上。可协商的子机制包括NTLM、Kerberos。当双方均不知道对方能使用/提供什么协议    的情况下，可以使用SP认证协议。目前，HttpCLient只支持Kerberos机制。

> 认证方案的方法重载有

> ![认证方案方法重载](https://git.oschina.net/uploads/images/2017/0707/173349_85fcd698_503982.png "认证方案方法重载")



### 执行请求

> 使用`RequestBase.execute`和`RequestBase.executeCallback`方法才是真正执行请求
> `execute`和`executeCallback`方法的不同之处在于前者只是使用默认提供的连接池来管理`HttpClient`,属于单个请求,而后者在前面的基础上增加的异步多线程请求。
> 返回值不同，执行`execute`方法返回`ResponseWrap`对象，`executeCallback`方法返回`RequestFuture`对象
> 其中`ResponseHandler`可以自定义返回值类型

> 对象方法重载如下
![请求方法重载](https://git.oschina.net/uploads/images/2017/0707/175112_89128b6d_503982.png "请求方法重载")


举个栗子

```java
HttpKit.get("https://0oky.com").execute().getString();

//多线程请求
List<RequestFuture<String>> taskWraps = new ArrayList<>();
RequestFuture<String> task;
RequestBase request = null;
			
for (int i = 0; i < 1000; i++) {
	request =  HttpKit.get("https://0oky.com"));
	task = request.executeCallback(executorService, new MyResponseHandler());
	taskWraps.add(task);
}
			
for (RequestFuture<String> taskWrap : taskWraps) {
    taskWrap.get(); //同步获取请求结果
}
			        
```


### ResponseWrap对象

> 执行`RequestBase.execute`方法将返回此对象。这个对象包含了对响应结果的处理，以及地址是否被重定向检查。处理的结果使用了`BufferedHttpEntity`对象进行缓存，所以响应结果是可重复读取的。(httpClient默认是不可重复读的，重复读将报错)
> 主要对结果处理的方法有`getByteArray`、`getBufferedReader`、`getString`、`transferTo` 、`getJson`和`getJsonList`